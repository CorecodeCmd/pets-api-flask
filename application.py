from flask import Flask


def create_app():
    # create and configure app
    app = Flask(__name__)
    app.config.from_pyfile('config.py')

    # import blueprints
    from pets.views import index
    from pets.views import pet_app

    # register blueprints
    app.register_blueprint(index)
    app.register_blueprint(pet_app)

    return app
