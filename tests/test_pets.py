from unittest import TestCase
import json

from database import storage
from application import create_app as create_app_base


class PetTest(TestCase):

    def create_app(self):
        return create_app_base()
    
    def setUp(self):
        self.app_factory = self.create_app()
        self.app = self.app_factory.test_client()
    
    def tearDown(self):
        storage.pets.clear()
    
    def test_empty_list(self):
        res = self.app.get('/pets/', content_type='application/json')
        self.assertIn('No pets yet', str(res.data))
        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(storage.pets), 0)
    
    def test_pet_not_found(self):
        res = self.app.get('/pets/1', content_type='application/json')
        self.assertEqual(res.status_code, 404)
    
    def test_post_pet(self):
        data = json.dumps({'name': 'Dog'})
        res = self.app.post('/pets/', data=data, content_type='application/json')
        self.assertEqual(res.status_code, 201)
        self.assertEqual(len(storage.pets), 1)
    
    def test_get_all_pets(self):
        data = json.dumps({'name': 'Dog'})
        self.app.post('/pets/', data=data, content_type='application/json')
        data = json.dumps({'name': 'Cat'})
        self.app.post('/pets/', data=data, content_type='application/json')
        res = self.app.get('/pets/', content_type='application/json')
        self.assertEqual(res.status_code, 200)
        self.assertIn('Cat', str(res.data))
        self.assertIn('Dog', str(res.data))
        self.assertEqual(len(storage.pets), 2)
