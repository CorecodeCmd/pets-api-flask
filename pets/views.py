from flask import Blueprint

from pets.api import PetAPI

index = Blueprint('index_view', __name__)

pet_app = Blueprint('pets_app', __name__)

pet_view = PetAPI.as_view('pets')

@index.route('/', methods=['GET',])
def home():
    return "Application works"

@index.route('/hello')
def hello_ladies():
    return "Hello Ladies"

pet_app.add_url_rule('/pets/', defaults={'pet_id': None},
                     view_func=pet_view, methods=['GET',])
pet_app.add_url_rule('/pets/', view_func=pet_view, methods=['POST',])
pet_app.add_url_rule('/pets/<int:pet_id>', view_func=pet_view,
                     methods=['GET', 'PUT', 'DELETE',])
