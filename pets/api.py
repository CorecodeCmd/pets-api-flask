from flask.views import MethodView
from flask import jsonify, request, abort

from database import storage

class PetAPI(MethodView):
    
    def get(self, pet_id):
        
        # check if pet_id is part of the endpoint url
        if pet_id:
            for pet in storage.pets:
                # if pet_id equals id of pet in database list, then return it
                if pet['id'] == pet_id:
                    return jsonify({'pet': storage.pets[pet_id - 1]}), 200
            else:
                # if no pet is found
                return jsonify({'error': f'pet with id {pet_id} not found'}), 404
        
        # if pet_id is not part of endpoint url, return all pets
        if not storage.pets:
            return jsonify({'pets': 'No pets yet'}), 200
        return jsonify({'pets': storage.pets}), 200
    
    def post(self):
        # adding pet to pets list
        if not request.json or not 'name' in request.json:
            abort(400)
        pet = {
            'id': len(storage.pets) + 1,
            'name': request.json['name'],
            'type': request.json['type'],
            'sex': request.json['sex'],
            'age': request.json['age']
        }
        storage.pets.append(pet)
        return jsonify({'pet': pet}), 201
    
    def put(self, pet_id):
        # updating pet
        if not request.json or not 'name' in request.json:
            abort(400)
        pet = storage.pets[pet_id - 1]
        pet['name'] = request.json['name']
        pet['type'] = request.json['type']
        pet['sex'] = request.json['sex']
        pet['age'] = request.json['age']
        return jsonify({'pet': pet}), 200
    
    def delete(self, pet_id):
        # deleting pet
        del storage.pets[pet_id - 1]
        return jsonify({}), 204
